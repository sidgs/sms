pipeline {
    agent any
    tools {
        maven 'Maven'
        nodejs 'NodeJS'
    }
    stages {
        stage('Clean') {
            steps {
                dir('edge') {
                    sh "mvn clean"
                }
            }
        }


        stage('Static Code Analysis, Unit Test and Coverage') {

            steps {
                dir('edge') {
                    sh "mvn test -Pproxy-unit-test "
                    sh "node node_modules/istanbul/lib/cli.js cover " +
                            "--dir target/unit-test-coverage " +
                            "node_modules/mocha/bin/_mocha test/unit"
                    echo "publish html report"
                    publishHTML(target: [
                            allowMissing         : false,
                            alwaysLinkToLastBuild: false,
                            keepAll              : true,
                            reportDir            : 'target/unit-test-coverage/lcov-report',
                            reportFiles          : 'index.html',
                            reportName           : 'Code Coverage HTML Report'
                    ])
                }
            }
        }



        stage('Linting') {
            steps {
                dir("edge") {


                    sh "apigeelint -s apiproxy -f html.js > target/unit-test-coverage/apigeelint.html"
                    echo "publish html report"
                    publishHTML(target: [
                            allowMissing         : false,
                            alwaysLinkToLastBuild: false,
                            keepAll              : true,
                            reportDir            : 'target/unit-test-coverage',
                            reportFiles          : 'apigeelint.html',
                            reportName           : 'Linting HTML Report'
                    ])
                }
            }
        }


        stage('Build proxy bundle') {
            steps {
                dir('edge') {

                    withCredentials([usernamePassword(credentialsId: "ferguson-edge-cred",
                            passwordVariable: 'apigee_pwd',
                            usernameVariable: 'apigee_user')]) {

                        sh "mvn package -Papigee -Denv=${params.apigee_env} -Dorg=${params.apigee_org}"+
                                " -Dusername=${apigee_user} -Dpassword=${apigee_pwd} "


                    }
                }
            }

        }



        stage('Pre-Deployment Configuration ') {
            steps {
                dir('edge') {

                    println "Predeployment of Caches "
                    withCredentials([usernamePassword(credentialsId: "ferguson-edge-cred",
                            passwordVariable: 'apigee_pwd',
                            usernameVariable: 'apigee_user')]) {
                        script {
                            if (fileExists("resources/edge/env/${params.apigee_env}/caches.json")) {
                                sh "mvn apigee-config:caches " +
                                        "    -Papigee -Denv=${params.apigee_env} -Dorg=${params.apigee_org} " +
                                        "    -Dusername=${apigee_user} " +
                                        "    -Dpassword=${apigee_pwd} "+
                                        "    -Dbearer=${bearer} -Drefresh=${refresh} " +
                                        "    -Dauthtype=${authtype} -Dtokenurl=${tokenurl} "
                            }


                            if (fileExists("resources/edge/env/${params.apigee_env}/kvms.json")) {
                                sh "mvn apigee-config:keyvaluemaps " +
                                        "    -Papigee -Denv=${params.apigee_env} -Dorg=${params.apigee_org} " +
                                        "    -Dusername=${apigee_user} " +
                                        "    -Dpassword=${apigee_pwd} "+
                                        "    -Dbearer=${bearer} -Drefresh=${refresh} " +
                                        "    -Dauthtype=${authtype} -Dtokenurl=${tokenurl} "
                            }

                            if (fileExists("resources/edge/env/${params.apigee_env}/targetServers.json")) {
                                sh "mvn apigee-config:targetservers " +
                                        "    -Papigee -Denv=${params.apigee_env} -Dorg=${params.apigee_org} " +
                                        "    -Dusername=${apigee_user} " +
                                        "    -Dpassword=${apigee_pwd} "+
                                        "    -Dbearer=${bearer} -Drefresh=${refresh} " +
                                        "    -Dauthtype=${authtype} -Dtokenurl=${tokenurl} "
                            }
                        }
                    }
                }
            }
        }


        stage('Deploy proxy bundle') {
            steps {
                dir('edge') {
                    withCredentials([usernamePassword(credentialsId: "ferguson-edge-cred",
                            passwordVariable: 'apigee_pwd',
                            usernameVariable: 'apigee_user')]) {

                        sh "mvn apigee-enterprise:deploy -Papigee -Denv=${params.apigee_env} -Dorg=${params.apigee_org}" +
                                " -Doptions=update -Dusername=${apigee_user} -Dpassword=${apigee_pwd} "+
                                " -Dbearer=${bearer} -Drefresh=${refresh} " +
                                " -Dauthtype=${authtype} -Dtokenurl=${tokenurl} "
                    }
                }
            }

        }


        stage('Post-Deployment Configurations for API ') {
            steps {
                dir('edge') {
                    withCredentials([usernamePassword(credentialsId: "ferguson-edge-cred",
                            passwordVariable: 'apigee_pwd',
                            usernameVariable: 'apigee_user')]) {

                        println "Post-Deployment Configurations for API Products Configurations, App Developer and App Configuration "

                        sh "mvn -Papigee -Denv=${params.apigee_env} -Dorg=${params.apigee_org} " +
                                " -Dapigee.config.options=create " +
                                " -Dusername=${apigee_user} -Dpassword=${apigee_pwd} " +
                                " -Dbearer=${bearer} -Drefresh=${refresh} " +
                                " -Dauthtype=${authtype} -Dtokenurl=${tokenurl} " +
                                "  apigee-config:apiproducts " +
                                "  apigee-config:developers apigee-config:apps apigee-config:exportAppKeys "
                    }
                }
            }

        }


        stage('Functional Test') {
                   steps {
                         dir('edge') {
                                   sh "node ./node_modules/cucumber/bin/cucumber.js target/test/integration/features --format json:target/reports.json"
                          }
                    }
              }






              stage('Functional Test Report') {
                     steps {
                           dir('edge') {
                                   step([
                                                  $class             : 'CucumberReportPublisher',
                                                  fileExcludePattern : '',
                                                  fileIncludePattern : "reports.json",
                                                  ignoreFailedTests  : false,
                                                  jenkinsBasePath    : '',
                                                  jsonReportDirectory: "target",
                                                  missingFails       : false,
                                                  parallelTesting    : false,
                                                  pendingFails       : false,
                                                  skippedFails       : false,
                                                  undefinedFails     : false
                                   ])
                           }
                     }
              }


        stage('upload-artifact') {

            steps {

                withMaven(maven: 'Maven',
                        globalMavenSettingsConfig: 'cicd-settings-file') {
                    sh "mvn deploy"
                }


            }
        }



    }
}

