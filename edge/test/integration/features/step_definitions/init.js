'use strict';

var apickli = require('apickli');
var config = require('../../test-config.json');
var uitility = require('./Utilities');
var {Before, Given, Then, When} = require('cucumber');

console.log('&$ver-demo-helloWorld: [' + config['ver-demo-helloWorld'].domain + ', ' + config['ver-demo-helloWorld'].basepath + ']');
Before(function(scenario, callback) {
		console.log("Clean Up ")
        var tokens = uitility.schemeSplit(config['ver-demo-helloWorld'].domain);
		this.apickli = new apickli.Apickli(tokens[0],
                                            tokens[1]+ config['ver-demo-helloWorld'].basepath,
										   './test/integration/features/fixtures/');
		callback();
	});

