var config = require('../../test-config.json');
var auth = require('../../auth-server.json');
var apps = require('../../devAppKeys.json');
var Promise = require('bluebird');
var utility= require('./Utilities');
var Given, Then, When, BeforeAll;
({Given, Then, When, BeforeAll} = require('cucumber'));


var creds={};
global.token=null;
function getCreds(appName, productName){

    for(var app in apps){
        if(apps[app].name === appName){
            var credentials = apps[app].credentials;
            for(var credential in credentials){
                var products = credentials[credential].apiProducts;
                for(var product in products){
                    if(products[product].apiproduct === productName){
                        creds.consumerKey = credentials[credential].consumerKey;
                        creds.consumerSecret = credentials[credential].consumerSecret;
                    }
                }
            }
        }
    }
}
function getToken(){
    return new Promise(function(resolve,reject){
        var request = require('apickli');
        var tokens=utility.schemeSplit(auth.domain);
        request= new request.Apickli(tokens[0],
            tokens[1] + auth.basepath,
            './test/integration/features/fixtures/');
        request.setRequestHeader("Authorization",'Basic '+Buffer.from(creds.consumerKey+':'+creds.consumerSecret).toString('base64'));
        request.post('/accesstoken?grant_type=client_credentials', function(error,response){
            if(response){
                token=JSON.parse(response.body).access_token;
                resolve();
            }
        });
    });
}


BeforeAll("BeforeFeatures", function(next) {
    if ( apps[0].name  ) {
        console.log ("Getting Creds from DevAppsKeys.json")
        getCreds(config['ver-demo-helloWorld'].app, config['ver-demo-helloWorld'].product);
    } else
    {
        console.log ("Getting Creds from creds.json")
        var credentials =require('../../creds.json')
        console.log("Creds: ", credentials)
        creds.consumerKey = credentials['key']
        creds.consumerSecret = credentials ['secret']
    }
    getToken().then(function(){
        return next();
    });
});

Given(/^standard oauth tokens are set$/, function (callback) {
    this.apickli.setRequestHeader("Authorization",'Bearer '+token);
    callback();
});