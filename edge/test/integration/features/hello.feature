Feature: Get Greeting Feature
	As an API user
	I want to get greetings

	Scenario: User should be addressed
		Given standard oauth tokens are set
		When I GET /json
		Then response code should be 200
		Then I should get the greetings

	Scenario: User should be notified of non-existing resource
		When I request a non-existing API resource
		Then I should see a resource not found response

