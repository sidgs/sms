


Instructions for ver-demo-helloWorld

[TOC levels=1-6]: # "# Table of Contents"

# Table of Contents
- [Reference API Proxy](#reference-api-proxy)
- [Description](#description)
- [Purpose](#purpose)
- [Pre-Requisites](#pre-requisites)
- [External configuration](#external-configuration)
    - [Access token generation](#access-token-generation)
        - [Acurl and get_token](#acurl-and-get_token)
    - [Maven cicd settings xml config](#maven-cicd-settings-xml-config)
- [Usage Instructions](#usage-instructions)
- [Lifecycle for API proxy deployment](#lifecycle-for-api-proxy-deployment)
    - [Lint Proxy](#lint-proxy)
    - [Unit Tests](#unit-tests)
    - [Bundle Package](#bundle-package)
    - [Upload Caches](#upload-caches)
    - [Upload Target Servers](#upload-target-servers)
    - [Upload KVM's](#upload-kvms)
    - [Deploy Proxy](#deploy-proxy)
    - [Upload Products](#upload-products)
    - [Upload Developers](#upload-developers)
    - [Upload Apps](#upload-apps)
    - [Export Keys](#export-keys)
    - [Integration Tests](#integration-tests)
    - [Upload to Artifactory](#upload-to-artifactory)




While deploying a API proxy to any targeted Apigee Edge env, it must go through with the several CI
phases such as Linting, Code Coverage, Unit testing, Build and packaging, configuring environment
level dependencies (KVM's, Caches, Target Servers), Configuring org level entities(API products,
Developers, Apps) and integration testing. Also, artifacts must be deploy to the remote locations
(Nexus).

    

The DevOps engineer should be able to execute all the above mentioned CI phases by manually running
the steps.

- JDK 8
- Apache Maven
- Linux/WSL
- Settings.xml file in local 
- SAML Tokens (Access & Refresh)



The external configuration references required for token generation and an external settings.xml

A document for the generating the token is given here:

https://bitbucket.org/sidgs/rbac-scripts/src/master/README.md

Go [here](https://ferguson.login.apigee.com/passcode) to generate temporary auth codes.


https://docs.apigee.com/api-platform/system-administration/auth-tools

Install `acurl` and `get_token`

```bash
# download and install acurl and get_token
curl https://login.apigee.com/resources/scripts/sso-cli/ssocli-bundle.zip -o "ssocli-bundle.zip"
unzip -n ssocli-bundle.zip
sudo ./install -b /usr/local/bin

# check installation
acurl -h
get_token -h
```

Use acurl to fetch refresh and access tokens:
```bash
# configure acurl
export SSO_LOGIN_URL=https://ferguson.login.apigee.com
export CLIENT_AUTH=ZWRnZWNsaTplZGdlY2xpc2VjcmV0

# run acurl; passcode is fetched from https://ferguson.login.apigee.com/passcode
acurl https://api.enterprise.apigee.com/v1/organization/ferguson-api -p $PASSCODE
```


A template for the cicd settings xml (which must be configured into Jenkins as a global config file)
is posted here:

https://bitbucket.org/sidgs/global-maven-settings/src/master/



This Api Proxy project will be used to deploy hello-world-api to apigee edge. 

How to clone the hello-world-api and change the directory 

```bash
# clone hello-world-api repository
git clone https://bitbucket.org/sidgs/hello-world-api.git

# change directory into the repository
cd hello-world-api
```


to make local invocation easier, load the app token from an environment variable

example of how to export a local environment variable:
```bash
export APIGEE_APITOKEN=abc123
```


```
# invoke apigeelint directly
apigeelint -s edge/apiproxy -f html.js

# invoke linting through maven; run from /edge directory in project root
cd edge
mvn test -Pproxy-linting             
```
 
```
# run from /edge directory in project root
mvn test -Pproxy-unit-test
```
 

run this from `edge` directory:
```bash
mvn package \
    -Papigee \
    -Denv=dev \
    -Dorg=ferguson-api \
    -DvhostProtocol=https \
    -DvhostDomainName=devapi2.ferguson.com \
    -DvhostDomainPort=443 \
    -DvhostEdgeName=fei-secure \
    -Dapigee.config.dir=target/resources/edge \
    -Dapigee.config.options=create \
    -Dapigee.config.exportDir=target/test/integration \
    -Dtokenurl=https://ferguson.login.apigee.com/oauth/token \
    -Dauthtype=oauth
```
 

```bash
mvn apigee-config:caches \
    -Papigee  \
    -Denv=dev \
    -Dorg=ferguson-api \
    -DvhostProtocol=https \
    -DvhostDomainName=devapi2.ferguson.com \
    -DvhostDomainPort=443 \
    -DvhostEdgeName=fei-secure \
    -Dapigee.config.dir=target/resources/edge \
    -Dapigee.config.options=create \
    -Dapigee.config.exportDir=target/test/integration \
    -Dapigee.api.port=8080 \
    -Dapigee.api.host=api.enterprise.apigee.com \
    -Dapigee.api.protocol=https \
    -Dtokenurl=https://ferguson.login.apigee.com/oauth/token \
    -Dauthtype=oauth \
    -Dbearer= ************* \
    -Drefresh= ***************
```
 

```bash
mvn apigee-config:targetservers \
    -Papigee \
    -Denv=dev \
    -Dorg=ferguson-api \
    -DvhostProtocol=https \
    -DvhostDomainName=devapi2.ferguson.com \
    -DvhostDomainPort=443 \
    -DvhostEdgeName=fei-secure \
    -Dapigee.config.dir=target/resources/edge \
    -Dapigee.config.options=create \
    -Dapigee.config.exportDir=target/test/integration \
    -Dapigee.api.port=8080 \
    -Dapigee.api.host=api.enterprise.apigee.com \
    -Dapigee.api.protocol=https \
    -Dtokenurl=https://ferguson.login.apigee.com/oauth/token \
    -Dauthtype=oauth \
    -Dbearer= ************* \
    -Drefresh= ***************
```
 

```bash
mvn apigee-config:keyvaluemaps \
    -Papigee \
    -Denv=dev \
    -Dorg=ferguson-api \
    -DvhostProtocol=https \
    -DvhostDomainName=devapi2.ferguson.com \
    -DvhostDomainPort=443 \
    -DvhostEdgeName=fei-secure \
    -Dapigee.config.dir=target/resources/edge \
    -Dapigee.config.options=create \
    -Dapigee.config.exportDir=target/test/integration \
    -Dapigee.api.port=8080 \
    -Dapigee.api.host=api.enterprise.apigee.com \
    -Dapigee.api.protocol=https \
    -Dtokenurl=https://ferguson.login.apigee.com/oauth/token \
    -Dauthtype=oauth \
    -Dbearer= ************* \
    -Drefresh= ***************
```
 

```bash
mvn apigee-enterprise:deploy \
    -Papigee \
    -Denv=dev \
    -Dorg=ferguson-api \
    -DvhostProtocol=https \
    -DvhostDomainName=devapi2.ferguson.com \
    -DvhostDomainPort=443 \
    -DvhostEdgeName=fei-secure \
    -Dapigee.config.dir=target/resources/edge \
    -Dapigee.config.options=create \
    -Dapigee.config.exportDir=target/test/integration \
    -Dapigee.api.port=8080 \
    -Dapigee.api.host=api.enterprise.apigee.com \
    -Dapigee.api.protocol=https \
    -Dtokenurl=https://ferguson.login.apigee.com/oauth/token \
    -Dauthtype=oauth \
    -Dbearer= ************* \
    -Drefresh= ***************
```
 

```bash
mvn apigee-config:apiproducts \
    -Papigee \
    -Denv=dev \
    -Dorg=ferguson-api \
    -DvhostProtocol=http \
    -DvhostDomainName=devapi2.ferguson.com \
    -DvhostDomainPort=443 \
    -DvhostEdgeName=fei-secure \
    -Dapigee.config.dir=target/resources/edge \
    -Dapigee.config.options=create \
    -Dapigee.config.exportDir=target/test/integration \
    -Dapigee.api.port=8080 \
    -Dapigee.api.host=api.enterprise.apigee.com \
    -Dapigee.api.protocol=https \
    -Dtokenurl=https://ferguson.login.apigee.com/oauth/token \
    -Dauthtype=oauth \
    -Dbearer= ************* \
    -Drefresh= ***************
```
 

```bash
mvn apigee-config:developers \
    -Papigee \
    -Denv=dev \
    -Dorg=ferguson-api \
    -DvhostProtocol=https \
    -DvhostDomainName=devapi2.ferguson.com \
    -DvhostDomainPort=443 \
    -DvhostEdgeName=fei-secure \
    -Dapigee.config.dir=target/resources/edge \
    -Dapigee.config.options=create \
    -Dapigee.config.exportDir=target/test/integration \
    -Dapigee.api.port=8080 \
    -Dapigee.api.host=api.enterprise.apigee.com \
    -Dapigee.api.protocol=https \
    -Dtokenurl=https://ferguson.login.apigee.com/oauth/token \
    -Dauthtype=oauth \
    -Dbearer= ************* \
    -Drefresh= ***************
```
 

```bash
mvn apigee-config:apps \
    -Papigee \
    -Denv=dev \
    -Dorg=ferguson-api \
    -DvhostProtocol=https \
    -DvhostDomainName=devapi2.ferguson.com \
    -DvhostDomainPort=443 \
    -DvhostEdgeName=fei-secure \
    -Dapigee.config.dir=target/resources/edge \
    -Dapigee.config.options=create \
    -Dapigee.config.exportDir=target/test/integration \
    -Dapigee.api.port=8080 \
    -Dapigee.api.host=api.enterprise.apigee.com \
    -Dapigee.api.protocol=https \
    -Dtokenurl=https://ferguson.login.apigee.com/oauth/token \
    -Dauthtype=oauth \
    -Dbearer= ************* \
    -Drefresh= ***************
```
 

```bash
mvn apigee-config:exportAppKeys \
    -Papigee \
    -Denv=dev \
    -Dorg=ferguson-api \
    -DvhostProtocol=https \
    -DvhostDomainName=devapi2.ferguson.com \
    -DvhostDomainPort=443 \
    -DvhostEdgeName=fei-secure \
    -Dapigee.config.dir=target/resources/edge \
    -Dapigee.config.options=create \
    -Dapigee.config.exportDir=target/test/integration \
    -Dapigee.api.port=8080 \
    -Dapigee.api.host=api.enterprise.apigee.com \
    -Dapigee.api.protocol=https 
 
    
```
 
 

```bash
node ./node_modules/cucumber/bin/cucumber.js target/test/integration/features --format json:target/reports.json

```


```bash
Disable for developers. Only CI Server can deploy to the remote location. 
```

